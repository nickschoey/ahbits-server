const moment = require('moment')
const schedule = require('node-schedule');
const sequelize = require('../db').sequelize;

module.exports.cutStreaks = async () => {
  const yesterday = moment().add(-1, 'days').format('YYYY-MM-DD');
  const brokenStreaks = await sequelize.query(`UPDATE activities SET currentStreak = 0 WHERE activities.id NOT IN(SELECT activityId FROM events WHERE createdAt LIKE '${yesterday}%') AND currentStreak > 1`)
  
  console.log(brokenStreaks[0].changedRows + ' streaks were cut');
  
}

module.exports.realCutStreaks = () => {
  schedule.scheduleJob('05 0 0 * * *', () => console.log('working!'));
  ;
}

module.exports.logServerInit = () => {
  console.log(`Server started at ${moment().format('HH:mm:ss')}`);
  console.log('Server listening on port 3000');
}