const eventService = require('./events.services');
const moment = require('moment')


module.exports.add = (req, res, next) => {
  
  eventService.add(req.body)
    .then((newEvent) => res.json({newEvent}))
    .catch(err => next(err));

}

module.exports.getPast = (req, res, next) => {
  eventService.getPast(req.user.sub)
    .then(pastEvents => res.json(pastEvents))
    .catch(err => next(err));
}

module.exports.getToday = (req, res, next) => {
  eventService.getToday(req.user.sub)
    .then(todayEvents => res.json(todayEvents))
    .catch(err => next(err));
}