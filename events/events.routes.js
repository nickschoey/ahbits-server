const express = require('express');
const router = express.Router();
const controller = require('./events.controller');

//routes
// router.get('/', controller.getAll);
router.get('/today', controller.getToday);
router.get('/past', controller.getPast);
router.post('/', controller.add);


module.exports = router;