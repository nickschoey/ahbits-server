const sequelize = require('../db').sequelize;
const Event = require('./event.model').Event;
const moment = require('moment');
const Sequelize = require('sequelize');

// ADD A NEW EVENT
module.exports.add = async (event) => {
  //adds the event
  await sequelize.sync()
    .then(() => Event.create({
      activityId: event.eventId,
    })
    )
  
    // ADD ONE TO THE CURRENT STREAK
  await sequelize.query(`UPDATE activities SET currentStreak = currentStreak + 1 WHERE ID=${event.eventId}`)

    // UPDATE THE HIGHEST STREAK
  await sequelize.query(`UPDATE activities SET highestStreak = currentStreak WHERE id=${event.eventId} AND currentStreak > highestStreak`);

  const newEvent = await sequelize.query(`SELECT * FROM activities WHERE id = ${event.eventId}`, { type: Sequelize.QueryTypes.SELECT });
  
  return newEvent;
  ;
}

// GET PAST EVENTS FOR A GIVEN USER
module.exports.getPast = async (userId) => {
  const today = moment().format('YYYY-MM-DD')
  const pastEvents = await sequelize.query(`SELECT events.createdAt, events.activityId, activities.name, activities.color, activities.userId, activities.highestStreak, activities.currentStreak FROM events LEFT JOIN activities ON activityId = activities.id WHERE userId = ${userId} AND NOT events.createdAt LIKE '${today}%';`, { type: Sequelize.QueryTypes.SELECT })

  return pastEvents;
}

// GET TODAY'S EVENTS FOR A GIVEN USER
module.exports.getToday = async (userId) => {
  const today = moment().format('YYYY-MM-DD')
  const todayEvents = await sequelize.query(`SELECT events.createdAt, events.activityId, activities.name, activities.color, activities.userId, activities.currentStreak, activities.highestStreak FROM events LEFT JOIN activities ON activityId = activities.id WHERE userId = ${userId} AND events.createdAt LIKE '${today}%';`, { type: Sequelize.QueryTypes.SELECT })

  return todayEvents;
}

// module.exports.getAll = async (userId) => {
  //   const activities = await Activity.findAll({ where: {userId: userId} });
//   const all = await Event.findAll({ where: {
//     activityId: {$in: activities.map(a => a.id)}
//   },
//     include: [{
//       model: Activity,
//       attributes: { exclude: ['createdAt', 'updatedAt', 'userId'] },
//       required: true,
//     }]
// });


//   return all;
// }

// module.exports.getToday = async (userId) => {
//   const activities = await Activity.findAll({ where: { userId: userId } });
//   const today = await Event.findAll({

//     include: [{
//       model: Activity,
//       attributes: {exclude: ['createdAt', 'updatedAt', 'userId']},
//       required: true,
//     }],

//     where: {
//       $and: [
//         { 
//           activityId: { $in: activities.map(a => a.id) },
//         },
//         sequelize.where(
//           sequelize.fn('DATE', sequelize.col('event.createdAt')),
//           sequelize.literal('CURRENT_DATE')
//         )
//       ]
//     }

//   });


//   return today;
// }
