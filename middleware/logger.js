const moment = require('moment')

const logger = (req, res, next) => {
  console.log('Request:', req.method, ' Route', req.path, ' Time:', moment().format('MMMM Do YYYY, HH:mm:ss'));
  
  next()
}


module.exports = logger;