const activityService = require('./activities.services');


module.exports.add = (req, res, next) => {
  activityService.add(req.body, req.user.sub)
    .then((activity) => {
      res.json(activity)
    })
    .catch(err => next(err));

}


module.exports.getAll = (req, res, next) => {
  activityService.getAll(req.user.sub)
    .then(activities => res.json(activities))
    .catch(err => next(err));
}


module.exports.getRemaining = (req, res, next) => {
  
  activityService.getRemaining(req.user.sub)
    .then(activities => {
      res.json(activities)

    })
    
    .catch(err => next(err));
}