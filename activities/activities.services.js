const sequelize = require('../db').sequelize;
const Activity = require('./activity.model').Activity;
const moment = require('moment')
const Sequelize = require('sequelize');
// ADD A NEW ACTIVITY FOR AN USER
module.exports.add = async (activity, id) => {
  
  // validate
  if (await Activity.findOne({ where: { name: activity.activityName, userId: id } })) {
    throw 'This activity already exists';
  }

  const newActivity = Activity.create({
      name: activity.activityName,
      color: activity.stickerColor,
      userId: id,
    })
  return newActivity;
  
}

// GET ALL ACTIVITIES FOR AN USER
module.exports.getAll = async (user) => {
  const all = Activity.findAll({ where: { userId: user } })
  return all;
}

// GET ACTIVITIES FOR AN USER THAT HAVEN'T BEEN YET EXECUTED TODAY
module.exports.getRemaining = async (user) => {
  const today = moment().format('YYYY-MM-DD');
  const remaining = sequelize.query(`SELECT * FROM activities WHERE activities.id NOT IN(SELECT activityId FROM events WHERE createdAt LIKE '${today}%') AND userId = ${user};`, { type: Sequelize.QueryTypes.SELECT })
  
  return remaining;
}