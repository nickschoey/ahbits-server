const express = require('express');
const router = express.Router();
const controller = require('./activities.controller');

//routes
router.post('/add', controller.add);
router.get('/', controller.getAll);
router.get('/remaining', controller.getRemaining);
// router.post('/register', controller.register);
// router.delete('/:id', controller._delete);

module.exports = router;