const Sequelize = require('sequelize');
const sequelize = require('../db').sequelize;

module.exports.Activity = sequelize.define('activity', {

  name: {
    type: Sequelize.STRING
  },
  color: {
    type: Sequelize.STRING
  },

  currentStreak: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  highestStreak: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  }
  
});
