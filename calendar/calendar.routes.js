const express = require('express');
const router = express.Router();
const controller = require('./calendar.controller');

// router.get('/week', controller.getweek);
router.get('/origin/:date', controller.getActive);
router.get('/today', controller.getToday);

module.exports = router;