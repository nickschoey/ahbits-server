const sequelize = require('../db').sequelize;
const Sequelize = require('sequelize');

module.exports.getActive = async (originDate) => {
  console.log(originDate);
  
  const calendar = await sequelize.query(`SELECT * FROM time_dimension WHERE db_date < CURRENT_DATE AND db_date >= CURRENT_DATE-10;`, { type: Sequelize.QueryTypes.SELECT })

  return calendar;
}