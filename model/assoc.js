const Activity = require('../activities/activity.model').Activity
const User = require('../users/user.model').User
const Event = require('../events/event.model').Event

Event.belongsTo(Activity);
Activity.belongsTo(User);
Activity.hasMany(Event);
User.hasMany(Activity);


