const express = require('express');
const router = express.Router();

//routes
router.get('/', (req, res, next) => {

  console.log('GET REQUEST SUCCESSFUL')
  res.sendStatus(200);
});
router.post('/', (req, res, next) => {
  
  console.log('POST REQUEST SUCCESS. DATA: ', req.body)
  res.sendStatus(200);
});

module.exports = router;