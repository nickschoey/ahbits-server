const express = require('express');
const router = express.Router();
const controller = require('./users.controller');

//routes
router.post('/login', controller.login);
router.post('/register', controller.register);
router.get('/', controller.getAll);
router.delete('/:id', controller._delete);

module.exports = router;
