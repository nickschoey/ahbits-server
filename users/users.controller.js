const userService = require('./users.services');


const register = (req, res, next) => {
  userService.create(req.body)
    .then(() => res.json({}))
    .catch(err => next(err));

}

const login = (req, res, next) => {
  userService.authenticate(req.body)
    .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password incorrect' }))
}

const getAll = (req, res, next) => {
  userService.getAll()
    .then(users => res.json(users))
    .catch(err => next(err));
}

const _delete = (req, res, next) => {
  userService._delete(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));

}
module.exports = {
  login,
  register,
  getAll,
  _delete
}