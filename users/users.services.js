const config = require('../config.json');
const jwt = require('jsonwebtoken');
const sequelize = require('../db').sequelize;
const User = require('./user.model').User;
const bcrypt = require('bcrypt');
const saltRounds = 10;

const create = async (user) => {
  //validate
  if (await User.findOne({ where: { email: user.email } })) {
    throw 'This email is already taken';
  }

  const salt = bcrypt.genSaltSync(saltRounds);
  await sequelize.sync()
    .then(() => User.create({

      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: bcrypt.hashSync(user.password, salt)
    })
    )
}

const authenticate = async ({ email, password }) => {
  const user = await User.findOne({ where: { email: email } });
  if (user && bcrypt.compareSync(password, user.password)) {

    const token = jwt.sign({ sub: user.id }, config.secret);
    const retObject = {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      createdAt: user.createdAt,
      token: token
    }
    return retObject;
  }
}

const getAll = async () => {
  const all = User.findAll({ attributes: { exclude: ['password'] } })

  return all;
}

const getById = async (id) => {
  const user = await User.findOne({ where: { id: id }, attributes: { exclude: ['password'] } });

  return user;
}



const _delete = async (id) => {
  await User.destroy({ where: { id: id }});
}

module.exports = {
  create,
  authenticate,
  getAll,
  getById,
  _delete

}