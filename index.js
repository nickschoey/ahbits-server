const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('./middleware/logger');
const jwt = require('./middleware/jwt');
const errorHandler = require('./middleware/error-handler');

const helpers = require('./helpers/helpers');
//run db
const db = require('./db');
require('./model/assoc');
db.sequelize.sync();

//middleware
app
  .use(bodyParser.json())
  .use(cors())
  .use(logger)
  .use(jwt())
  // routes
  .use('/test', require('./test.routes'))
  .use('/users', require('./users/users.routes'))
  .use('/activity', require('./activities/activities.routes'))
  .use('/events', require('./events/events.routes'))
  .use('/calendar', require('./calendar/calendar.routes'))
  //global error handler
  .use(errorHandler)

// Start server and update DB
app
  .listen(3000, helpers.logServerInit)
  .on('listening', helpers.cutStreaks)